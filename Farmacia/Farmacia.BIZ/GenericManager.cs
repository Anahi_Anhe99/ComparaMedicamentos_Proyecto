﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        IGenericRepository<T> repository;
        public GenericManager(IGenericRepository<T> genericRepository)
        {
            repository = genericRepository;
        }
        public List<T> Listar => repository.Read.OrderBy(p => p.Nombre).ToList();

        public string Error { get => repository.Error; set { } }

        public bool Crear(T entidad) => repository.Create(entidad);

        public bool Eliminar(ObjectId id) => repository.Delete(id);

        public bool Modificar(T entidad) => repository.Update(entidad);
    }
}
