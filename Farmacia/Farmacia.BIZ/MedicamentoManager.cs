﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.BIZ
{
    public class MedicamentoManager : GenericManager<Medicamento>, IMedicamentoManager
    {
        public MedicamentoManager(IGenericRepository<Medicamento> genericRepository) : base(genericRepository)
        {
        }
    }
}
