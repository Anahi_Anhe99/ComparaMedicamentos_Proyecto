﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.BIZ
{
    public class EstablecimientoManager : GenericManager<Establecimiento>, IEstablecimientoManager
    {
        public EstablecimientoManager(IGenericRepository<Establecimiento> genericRepository) : base(genericRepository)
        {
        }
    }
}
