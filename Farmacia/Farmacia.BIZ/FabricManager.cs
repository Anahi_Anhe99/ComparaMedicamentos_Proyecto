﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Validadores;
using Farmacia.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.BIZ
{
    public static class FabricManager
    {
        public static EstablecimientoManager Establecimiento()
        {
            return new EstablecimientoManager(new GenericRepository<Establecimiento>(new EstablecimientoValidator()));
        }

        public static MedicamentoManager Medicamento()
        {
            return new MedicamentoManager(new GenericRepository<Medicamento>(new MedicamentoValidator()));
        }

        public static UsuarioManager Usuario()
        {
            return new UsuarioManager(new GenericRepository<Usuario>(new UsuarioValidator()));
        }
    }
}
