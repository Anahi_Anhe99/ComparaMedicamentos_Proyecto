﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farmacia.UI.Medicamento
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            HabilitarBotones(true);
            HabilitarCampos(false);
            LimpiarCajas();
        }

        private void HabilitarCampos(bool v)
        {
            txbNombreCom_Medicamento.IsEnabled = v;
            txbNombreGen_Medicamento.IsEnabled = v;
            txbPrecio.IsEnabled = v;
            txbNombre_Laboratorio.IsEnabled = v;
            cmbNombre_Farmacia.IsEnabled = v;
        }

        public enum Accion
        {
            Nuevo,
            Editar
        }
        Accion accion;
        private void LimpiarCajas()
        {
            txbNombreCom_Medicamento.Clear();
            txbNombreGen_Medicamento.Clear();
            txbPrecio.Clear();
            txbNombre_Laboratorio.Clear();
            cmbNombre_Farmacia.Text = "";
        }

        private void HabilitarBotones(bool v)
        {
            btnNuevo.IsEnabled = v;
            btnGuardar.IsEnabled = !v;
        }
        private void btnCargarImagen_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            HabilitarBotones(false);
            accion = Accion.Nuevo;
            HabilitarCampos(true);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            Medicamento m = new Medicamento
            {
                Nombre = txbNombreGen_Medicamento.Text,
                NombreComercial = txbNombreCom_Medicamento.Text,
                NombreLaboratorio = txbNombre_Laboratorio.Text,
                EsGenerico = chkEsGenerico.IsChecked.Value,
                Precio = float.Parse(txbPrecio.Text),
                //Imagen=

            };
            HabilitarCampos(false);
        }
    }
}
