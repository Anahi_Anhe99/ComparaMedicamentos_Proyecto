﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidades
{
    public class Usuario : BaseDTO
    {
        public string IdSkype { get; set; }
        public string IdTelegram { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
