﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidades
{
    public class Establecimiento : BaseDTO
    {
        public string Direccion { get; set; }
    }
}
