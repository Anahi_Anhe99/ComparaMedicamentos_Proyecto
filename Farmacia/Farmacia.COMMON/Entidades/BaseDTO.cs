﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidades
{
    public abstract class BaseDTO
    {
        public ObjectId Id { get; set; }
        public string Nombre { get; set; }
    }
}
