﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidades
{
    public class Medicamento : BaseDTO
    {
        public ObjectId IdFarmacia { get; set; }
        public string NombreComercial { get; set; }
        public float Precio { get; set; }
        public bool EsGenerico { get; set; }
        public string NombreFarmacia { get; set; }
        public byte Imagen { get; set; }
        public ObjectId IdUsuario { get; set; }
        public string NombreLaboratorio { get; set; }
    }
}
