﻿using Farmacia.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T : BaseDTO
    {
        string Error { get; set; }
        bool Resultado { get; }
        bool Create(T entidad);
        List<T> Read { get; }
        bool Update(T entidad);
        bool Delete(ObjectId id);
    }
}
