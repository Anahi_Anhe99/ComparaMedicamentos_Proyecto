﻿using Farmacia.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Interfaces
{
    public interface IGenericManager<T> where T : BaseDTO
    {
        bool Crear(T entidad);
        List<T> Listar { get; }
        bool Modificar(T entidad);
        bool Eliminar(ObjectId id);
        string Error { get; set; }
    }
}
