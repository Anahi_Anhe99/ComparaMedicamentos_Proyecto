﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Validadores
{
    public class EstablecimientoValidator : GenericValidator<Establecimiento>
    {
        public EstablecimientoValidator()
        {
            RuleFor(e => e.Direccion).NotNull().NotEmpty().Length(3, 300);
        }
    }
}
