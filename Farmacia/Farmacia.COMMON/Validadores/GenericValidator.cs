﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Validadores
{
    public class GenericValidator<T> : AbstractValidator<T> where T : BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(e => e.Nombre).NotNull().NotEmpty().Length(3, 200);
            RuleFor(e => e.Id).NotNull().NotEmpty();
        }
    }
}
