﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Validadores
{
    public class UsuarioValidator : GenericValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(e => e.IdSkype).NotNull().NotEmpty();
            RuleFor(e => e.IdTelegram).NotNull().NotEmpty();
            RuleFor(e => e.Password).NotNull().NotEmpty().Length(8, 50);
            RuleFor(e => e.Email).NotNull().NotEmpty().Length(3, 50).EmailAddress();
        }
    }
}
