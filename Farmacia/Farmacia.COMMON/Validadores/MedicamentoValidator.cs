﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Validadores
{
    public class MedicamentoValidator : GenericValidator<Medicamento>
    {
        public MedicamentoValidator()
        {
            RuleFor(e => e.IdFarmacia).NotNull().NotEmpty();
            RuleFor(e => e.Precio).NotNull().NotEmpty();
            RuleFor(e => e.NombreFarmacia).NotNull().NotEmpty().Length(3, 200);
            RuleFor(e => e.NombreComercial).NotNull().NotEmpty().Length(3, 200);
            RuleFor(e => e.IdUsuario).NotNull().NotEmpty();
        }
    }
}
