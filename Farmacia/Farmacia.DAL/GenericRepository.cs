﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using FluentValidation;
using FluentValidation.Results;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private MongoClient client;
        private IMongoDatabase db;
        private AbstractValidator<T> Validator;
        public string Error { get; set; }
        public bool Resultado { get; private set; }
        private ValidationResult validationResult;
        public GenericRepository(AbstractValidator<T> validator)
        {
            client = new MongoClient(new MongoUrl("mongodb://cf:cf@ds014648.mlab.com:14648/comparafarmaciasdb"));
            db = client.GetDatabase("comparafarmaciasdb");
            Validator = validator;
        }

        private IMongoCollection<T> Collection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }

        public List<T> Read => Collection().AsQueryable().ToList();

        public bool Create(T entidad)
        {
            entidad.Id = ObjectId.GenerateNewId();
            validationResult = Validator.Validate(entidad);
            if (validationResult.IsValid)
            {
                try
                {
                    Collection().InsertOne(entidad);
                    Resultado = true;
                    Error = null;
                }
                catch (Exception)
                {
                    Error = "No se pudo crear " + typeof(T).Name;
                    Resultado = false;
                }
            }
            else
            {
                Resultado = false;
                ObtenerError(validationResult.Errors);
            }
            return Resultado;
        }

        public bool Delete(ObjectId id)
        {
            try
            {
                Resultado = Collection().DeleteOne(e => e.Id == id).DeletedCount == 1;
                Error = null;
            }
            catch (Exception)
            {
                Resultado = false;
                Error = "No se pudo eliminar " + typeof(T).Name;
            }
            return Resultado;
        }

        public bool Update(T entidad)
        {
            validationResult = Validator.Validate(entidad);
            if (validationResult.IsValid)
            {
                try
                {
                    Resultado = Collection().ReplaceOne(p => p.Id == entidad.Id, entidad).ModifiedCount == 1;
                    Error = null;
                }
                catch (Exception)
                {
                    Error = "No se pudo modificar " + typeof(T).Name;
                    Resultado = false;
                }
            }
            else
            {
                Resultado = false;
                ObtenerError(validationResult.Errors);
            }
            return Resultado;
        }

        private void ObtenerError(IList<ValidationFailure> errors)
        {
            Error = typeof(T).Name + " Invalido/a";
            foreach (var error in errors)
            {
                Error += "\n" + error.ErrorMessage;
            }
        }
    }
}
